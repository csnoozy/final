/*
 Caleb Snoozy
 Due Date: 3/20/2017
 Assignment: Final Project
 File: generator.js
 Description: Handles all the logic for /final/gen/ , creating wallpapers using math and the canvas,
*/

/**
 * Abstract generator class, used for "type-safety".
 * Extend this to create a new Generator.
 * TODO(Caleb S.): Dynamically create generator list options when the constructor is called.
 */
class Generator {
    /**
     * @param {string} name - Used for console logging.
     */
    constructor(name) {
        this.name = name;
        if (new.target === Generator) {
            throw new TypeError("Cannot construct Generator instances directly");
        }
        if (this.draw === undefined) {
            throw new TypeError("Must override the draw method");
        }
        if (this.updateStatus === undefined) {
            throw new TypeError("Must override the updateStatus method");
        }
        if (this.init === undefined) {
            throw new TypeError("Must override the init method");
        }
        if (this.finished === undefined) {
            throw new TypeError("Must override the finished method");
        }
        if (this.loadOptions === undefined) {
            throw new TypeError("Must override the loadOptions method");
        }
    }
}

/**
 * A Mandelbrot set visualization, using GPU.js to quickly generate points, then drawing with canvas.
 * Allows for custom iterations and setting the width and height bounds for custom zooming.
 * @extends Generator
 * TODO(Caleb S.): Implement click and drag zooming to rectangle select area.
 * TODO(Caleb S.): Use GPU.js render to canvas to draw faster.
 */
class Mandelbrot extends Generator {
    /** Setup defaults for the Mandelbrot generator. */
    constructor() {
        super("iterative-mandelbrot");
        // Used for runtime, reset on init
        this.iter = 0;
        this.results = new Float32Array(width * height);
        this.resultsHist = new Float32Array(width * height);
        this.startTime = 0;
        this.status = "rendering";
        this.percentage = 0;

        // Manipulable variables.
        this.maxIterations = 50;
        this.xMinBounds = -2.5;
        this.xMaxBounds = 1;
        this.yMinBounds = -1;
        this.yMaxBounds = 1;
    }

    /** Called when this generator is selected in Generation Selection to load it's customizations. */
    loadOptions() {
        // HTML Options cached by JQuery.
        let $inputIterations, $rangeMinX, $spanMinX, $rangeMaxX, $spanMaxX, $rangeMinY, $spanMinY, $rangeMaxY,
            $spanMaxY;

        // Fill the Options area with the required inputs.
        $divOptions.html(
            createInput("max Iterations", "gen-input-iterations", this.maxIterations, 1, 1000000, 1) +
            createInputRange("X Min", "gen-range-x-min", "gen-span-x-min", -2.5, -2.5, 1, 0.01) +
            createInputRange("X Max", "gen-range-x-max", "gen-span-x-max", 1, -2.5, 1, 0.01) +
            createInputRange("Y Min", "gen-range-y-min", "gen-span-y-min", -1, -1, 1, 0.01) +
            createInputRange("Y Max", "gen-range-y-max", "gen-span-y-max", 1, -1, 1, 0.01)
        );

        // Cache newly created elements.
        $inputIterations = $('#gen-input-iterations');
        $rangeMinX = $('#gen-range-x-min');
        $spanMinX = $('#gen-span-x-min');
        $rangeMaxX = $('#gen-range-x-max');
        $spanMaxX = $('#gen-span-x-max');
        $rangeMinY = $('#gen-range-y-min');
        $spanMinY = $('#gen-span-y-min');
        $rangeMaxY = $('#gen-range-y-max');
        $spanMaxY = $('#gen-span-y-max');


        // Create on change events for the inputs.
        $inputIterations.on('change', () => {
            this.maxIterations = Number($inputIterations.val());
        });

        $rangeMinX.on("change mousemove", () => {
            $spanMinX.html(Number($rangeMinX.val()).toFixed(2));
            this.xMinBounds = Number($rangeMinX.val());
        });
        $rangeMaxX.on("change mousemove", () => {
            $spanMaxX.html(Number($rangeMaxX.val()).toFixed(2));
            this.xMaxBounds = Number($rangeMaxX.val());
        });
        $rangeMinY.on("change mousemove", () => {
            $spanMinY.html(Number($rangeMinY.val()).toFixed(2));
            this.yMinBounds = Number($rangeMinY.val());
        });
        $rangeMaxY.on("change mousemove", () => {
            $spanMaxY.html(Number($rangeMaxY.val()).toFixed(2));
            this.yMaxBounds = Number($rangeMaxY.val());
        });
    }

    /**
     * Called when the Generate button is pressed.
     * @param {number} width - Width of the render, not the canvas.
     * @param {number} height - Height of the render, not the canvas.
     */
    init(width, height) {
        console.log(this.name + " init");

        // Needs to be reset on init.
        this.iter = 0;
        this.results = new Float32Array(width * height);
        this.resultsHist = new Float32Array(width * height);
        this.status = "generating";
        this.percentage = 0;

        this.width = width;
        this.height = height;

        /**
         * Part 1
         * Creates Positional correct points.
         *
         * Create the gpu "shader" to generate the Mandelbrot set.
         * NOTE: All variables created within the "shader" are always of type float.
         * @param {number} xMinBounds - X minimum bounds of the render.
         * @param {number} xMaxBounds - X maximum bounds of the render.
         * @param {number} yMinBounds - Y minimum bounds of the render.
         * @param {number} yMaxBounds - y maximum bounds of the render.
         * @param {number} maxIterations - Maximum iterations to render per pixel.
         *
         * Formula was adapted from pseudocode from the Mandelbrot Set wikipidia page
         * @tutorial https://en.wikipedia.org/wiki/Mandelbrot_set
         *
         * .dimensions([width, height]) - dimensions of render in pixels.
         * .loopMaxIterations({number} loops) - Maximum number of allowed iterations for the shader, must be higher
         *                                      than desired iterations or will cause unintended limits.
         *
         * @return {Float32Array} Results of the shader.
         */
        this.gpuMandelbrot = gpu.createKernel(function (xMinBounds, xMaxBounds, yMinBounds, yMaxBounds, maxIterations) {
            /**
             * Map an input from a range to a new range.
             * @param {float} input - Value to be mapped.
             * @param {float} minInput - Minimum value of input.
             * @param {float} maxInput - Maximum value of input.
             * @param {float} minOutput - Minimum mapped value.
             * @param {float} maxOutput - Maximum mapped value.
             * @return {float} Mapped value.
             */
            function map(input, minInput, maxInput, minOutput, maxOutput) {
                return (input - minInput) * (maxOutput - minOutput) / (maxInput - minInput) + minOutput;
            }

            // Mandelbrot Calculation
            var x0 = map(this.thread.x, 0, this.dimensions.x, xMinBounds, xMaxBounds); // Scaled X coordinate of pixel
            var y0 = map(this.thread.y, 0, this.dimensions.y, yMinBounds, yMaxBounds); // Scaled Y corrdinate of pixel
            var x = 0.0;
            var y = 0.0;
            var iteration = 0;
            var max_iteration = maxIterations;
            for (var iter = 0; iter < max_iteration; iter++) {
                if (!(x * x + y * y < Math.pow(2, 16))) // If coordinates are escaping toward Infinity
                    break;
                var xtemp = x * x - y * y + x0;
                y = 2 * x * y + y0;
                x = xtemp;
                iteration++;
            }

            // Continuous (Smooth) coloring
            if (iteration < max_iteration) {
                // sqrt of inner term removed using log simplification rules.
                var log_nz = Math.log(x * x + y * y) / 2;
                var nu = Math.log(log_nz / Math.log(2)) / Math.log(2);
                // Rearranging the potential function.
                // Dividing log_zn by log(2) instead of log(N = 1<<8)
                // because we want the entire palette to range from the
                // center to radius 2, NOT our bailout radius.
                iteration = iteration + 1 - nu;
            }

            return iteration;
        }).dimensions([this.width, this.height]).loopMaxIterations(1000000);

        // Begin timing the generation
        this.startTime = performance.now();

        // Call shader for data
        let fromGPU = this.gpuMandelbrot(this.xMinBounds, this.xMaxBounds, this.yMinBounds, this.yMaxBounds, this.maxIterations);
        // Fix results into a single array.
        this.results.set(fromGPU[0]);
        this.results.set(fromGPU[1], this.width);

        /**
         * Part 2
         * Creates color correct points for use with histogram.
         *
         * Create the gpu "shader" to generate the Mandelbrot set.
         * NOTE: All variables created within the "shader" are always of type float.
         * @param {number} xMinBounds - X minimum bounds of the render.
         * @param {number} xMaxBounds - X maximum bounds of the render.
         * @param {number} yMinBounds - Y minimum bounds of the render.
         * @param {number} yMaxBounds - y maximum bounds of the render.
         * @param {number} maxIterations - Maximum iterations to render per pixel.
         *
         * Formula was adapted from pseudocode from the Mandelbrot Set wikipidia page
         * @tutorial https://en.wikipedia.org/wiki/Mandelbrot_set
         *
         * .dimensions([width, height]) - dimensions of render in pixels.
         * .loopMaxIterations({number} loops) - Maximum number of allowed iterations for the shader, must be higher
         *                                      than desired iterations or will cause unintended limits.
         *
         * @return {Float32Array} Results of the shader.
         */
        this.gpuMandelbrot = gpu.createKernel(function (xMinBounds, xMaxBounds, yMinBounds, yMaxBounds, maxIterations) {

            /**
             * Map an input from a range to a new range.
             * @param {float} input - Value to be mapped.
             * @param {float} minInput - Minimum value of input.
             * @param {float} maxInput - Maximum value of input.
             * @param {float} minOutput - Minimum mapped value.
             * @param {float} maxOutput - Maximum mapped value.
             * @return {float} Mapped value.
             */
            function map(input, minInput, maxInput, minOutput, maxOutput) {
                return (input - minInput) * (maxOutput - minOutput) / (maxInput - minInput) + minOutput;
            }

            // Mandelbrot Calculation
            var x0 = map(this.thread.x, 0, this.dimensions.x, xMinBounds, xMaxBounds); // Scaled X coordinate of pixel
            var y0 = map(this.thread.y, 0, this.dimensions.y, yMinBounds, yMaxBounds); // Scaled Y corrdinate of pixel
            var x = 0.0;
            var y = 0.0;
            var iteration = 0;
            var max_iteration = maxIterations;
            for (var iter = 0; iter < max_iteration; iter++) {
                if (!(x * x + y * y < Math.pow(2, 16)))
                    break;
                var xtemp = x * x - y * y + x0;
                y = 2 * x * y + y0;
                x = xtemp;
                iteration++;
            }

            return iteration;
        }).dimensions([this.width, this.height]).loopMaxIterations(1000000);

        // Call shader for data
        let fromGPUHist = this.gpuMandelbrot(this.xMinBounds, this.xMaxBounds, this.yMinBounds, this.yMaxBounds, this.maxIterations);
        // Fix results to single array
        this.resultsHist.set(fromGPUHist[0]);
        this.resultsHist.set(fromGPUHist[1], this.width);

        // Fill canvas with black
        context.fillStyle = '#000000';
        context.fillRect(0, 0, width, height);

        // Create Histogram data.
        this.status = "histogram";
        this.updateStatus();
        this.histogram = new Array(this.maxIterations);
        this.histogram.fill(0);
        for (let i = 0; i < this.resultsHist.length; i++) {
            this.histogram[this.resultsHist[i]] += 1;
        }

        // Get Histogram total.
        this.status = "pre rendering";
        this.updateStatus();
        this.total = 0.0;
        for (let i = 0; i < this.maxIterations; i++) {
            this.total += this.histogram[i];
        }

        // Begin rendering.
        requestAnimationFrame(() => {
            this.draw()
        });
    }

    /**
     * Render loop.
     */
    draw() {
        this.status = "rendering";
        for (let i = 0; i < this.width; i++) {
            let temp = this.results[this.iter];

            let hue1 = 0.0;
            let hue2 = 0.0;
            for (let o = 0; o < Math.floor(temp); o++) {
                hue1 += this.histogram[o] / this.total;
            }
            for (let o = 0; o < Math.floor(temp) + 1; o++) {
                hue2 += this.histogram[o] / this.total;
            }
            let color = Math.floor(lerp(hue1, hue2, temp % 1).map(0, 1, 255, 0));
            if (color < 254)
                context.fillStyle = `hsl(${color}, 50%, 50%)`;
            else
                context.fillStyle = 'hsl(0, 0%, 0%)';
            context.fillRect((this.iter % this.width), Math.floor(this.iter / this.width), 1, 1);
            if (this.iter < this.results.length) {
                this.percentage = this.iter / this.results.length * 100;
                this.iter++;
            }
        }

        this.updateStatus();
        if (this.iter < this.results.length) {
            requestAnimationFrame(() => {
                this.draw()
            });
        } else {
            this.finished();
        }
    }

    /**
     * Update the progress span.
     */
    updateStatus() {
        $badgeProgress.html(`${this.status} ${this.percentage.toFixed(0).toString()}%`);
    }

    /**
     * Called when generation is finished.
     */
    finished() {
        console.log(this.name + " Finished " + ((performance.now() - this.startTime) / 1000).toFixed(2) + "s");
        this.status = "finished";
        this.updateStatus();

        $btnGenerate[0].disabled = false;
        $btnSave[0].disabled = false;
    }
}

/**
 * A Recursive pattern visualization.
 * @extends Generator
 */
class Weave extends Generator {
    /** Setup defaults for the Weave generator. */
    constructor() {
        super("chaos-weave");
        // Used for runtime, reset on init
        this.startTime = 0;
        this.status = "rendering";
        this.percentage = 0;
        this.maxSquares = 100000;
    }

    /** Called when this generator is selected in Generation Selection to load it's customizations. */
    loadOptions() {
        $divOptions.html(``);
    }

    /**
     * Called when the Generate button is pressed.
     * @param {number} width - Width of the render, not the canvas.
     * @param {number} height - Height of the render, not the canvas.
     */
    init(width, height) {
        console.log(this.name + " init");

        // Needs to be reset on init
        this.iter = 0;
        this.status = "generating";
        this.percentage = 0;

        this.width = width;
        this.height = height;

        this.startTime = performance.now();

        context.fillStyle = '#000000';
        context.fillRect(0, 0, width, height);

        this.updateStatus();

        this.iterationSpeed = 10;
        this.squares = 0;

        requestAnimationFrame(() => {
            this.draw()
        });
    }

    /**
     * Render loop.
     */
    draw() {
        this.status = "rendering";

        let loop = performance.now();
        for (let i = 0; i < this.iterationSpeed; i++) {
            this.weavingLines(getRandomInt(0, this.width), getRandomInt(0, this.height));
            this.squares++;
        }
        let loopEnd = performance.now();
        if (loopEnd - loop < 150) {
            this.iterationSpeed++;
        }

        this.percentage = this.squares / this.maxSquares * 100;
        this.updateStatus();
        if (this.squares < this.maxSquares) {
            requestAnimationFrame(() => {
                this.draw();
            });
        } else {
            this.finished();
        }
    }

    /**
     * Update the progress span.
     */
    updateStatus() {
        $badgeProgress.html(`${this.status} ${this.percentage.toFixed(0).toString()}%`);
    }

    /**
     * Called when generation is finished.
     */
    finished() {
        console.log(this.name + " Finished " + ((performance.now() - this.startTime) / 1000).toFixed(2) + "s");
        this.status = "finished";
        this.updateStatus();

        $btnGenerate[0].disabled = false;
        $btnSave[0].disabled = false;
    }

    /**
     * Recursive drawing pattern
     */
    weavingLines(x, y, depth = 0) {
        let r, g, b, prev;
        prev = context.getImageData(x, y, 1, 1).data;
        r = prev[0] + Math.floor(15 * Math.sin(x / width)) + getRandomInt(-15, 15);
        g = prev[1] + Math.floor(15 * Math.sin(y / height)) + getRandomInt(-15, 15);
        b = prev[2] + Math.floor(5 * Math.sin(depth / 100)) + getRandomInt(-15, 15);
        context.fillStyle = `rgba(${r}, ${g}, ${b}, 255)`;
        context.fillRect(x, y, 1, 1);
        if (depth < 100) {
            this.weavingLines(x + Math.floor(Math.sin(x / (50 + Math.random()))), y + Math.floor(Math.sin(y / (50 + Math.random()))), depth + 1);
        }
    }
}

/**
 * A Circle Packing visualization.
 * Allows custom circle sizing, color, and whether to allow circles to generate in circles.
 * @extends Generator
 */
class CirclePacked extends Generator {
    /** Setup defaults for the CirclePacked generator. */
    constructor() {
        super('chaos-circle-pack');
        this.startTime = 0;
        this.status = "rendering";
        this.percentage = 0;
        this.maxCircles = 100000;
        this.minSize = 10;
        this.maxSize = 255;
        this.inside = false;
        this.backgroundColor = '#000000';
        this.invertColor = false;
        this.randomColor = false;
        this.mainColor = tinycolor.random();
    }

    /** Called when this generator is selected in Generation Selection to load it's customizations. */
    loadOptions() {
        // JQuery cached elements
        let $maxCircles, $inside, $backgroundColor, $maxSize, $minSize, $mainColor, $invertColor, $randomColor;

        // Create input Elements
        $divOptions.html(
            createInput("Max Iterations", "gen-input-max-circles", this.maxCircles, 1, 1000000, 1) +
            createInput("Max Size", "gen-input-max-size", this.maxSize, 1, 100000) +
            createInput("Min Size", "gen-input-min-size", this.minSize, 0, 100000) +
            createCheckbox("Fill Inside", "gen-checkbox-in", this.inside) +
            createColorInput("Background Color", "gen-color-input", this.backgroundColor) +
            createCheckbox("Invert Color", "gen-checkbox-invert", this.invertColor) +
            createCheckbox("Random Color", "gen-checkbox-random", this.randomColor) +
            createColorInput("Circle Color", "gen-color-main", '#' + this.mainColor.toHex())
        );

        // Cache elements
        $maxCircles = $('#gen-input-max-circles');
        $maxSize = $('#gen-input-max-size');
        $minSize = $('#gen-input-min-size');
        $backgroundColor = $('#gen-color-input');
        $invertColor = $('#gen-checkbox-invert');
        $mainColor = $('#gen-color-main');
        $randomColor = $('#gen-checkbox-random');
        $inside = $('#gen-checkbox-in');

        // Create on change events for input elements.
        $maxCircles.on('change', () => {
            this.maxCircles = Number($maxCircles.val());
        });

        $maxSize.on('change', () => {
            this.maxSize = Number($maxSize.val());
        });

        $minSize.on('change', () => {
            this.minSize = Number($minSize.val());
        });

        $inside.on('change', () => {
            this.inside = $inside[0].checked;
        });

        $backgroundColor.on('change', () => {
            this.backgroundColor = $backgroundColor.val();
        });

        $invertColor.on('change', () => {
            this.invertColor = $invertColor[0].checked;
        });

        $randomColor.on('change', () => {
            this.randomColor = $randomColor[0].checked;
        });

        $mainColor.on('change', () => {
            this.mainColor = tinycolor($mainColor.val());
        });


    }

    /**
     * Called when the Generate button is pressed.
     * @param {number} width - Width of the render, not the canvas.
     * @param {number} height - Height of the render, not the canvas.
     */
    init(width, height) {
        console.log(this.name + " init");

        // Needs to be reset on init
        this.iter = 0;
        this.status = "generating";
        this.percentage = 0;

        this.width = width;
        this.height = height;

        this.circles = 0;
        this.circleArray = [];

        // Start timing
        this.startTime = performance.now();

        // Fill screen with background color
        context.fillStyle = this.backgroundColor;
        context.fillRect(0, 0, width, height);

        this.updateStatus();

        // begin render loop
        requestAnimationFrame(() => {
            this.draw()
        });
    }

    /**
     * Render loop.
     */
    draw() {
        this.status = "rendering";
        for (let i = 0; i < (this.maxCircles < 1000 ? this.maxCircles : 1000); i++) {
            let r = this.maxSize;
            let x = getRandomInt(0, this.width);
            let y = getRandomInt(0, this.height);
            if (this.circleArray.length === 0) {
                this.circleArray.push({x: x, y: y, r: r});
            } else {
                for (let temp of this.circleArray) {
                    let distance = Math.sqrt(Math.pow(temp.x - x, 2) + Math.pow(temp.y - y, 2));
                    if (this.inside) {
                        r = Math.abs(distance - temp.r) < r ? Math.floor(Math.abs(distance - temp.r)) : Math.floor(r);
                    } else {
                        if (distance - temp.r >= 0)
                            r = Math.abs(distance - temp.r) < r ? Math.floor(Math.abs(distance - temp.r)) : Math.floor(r);
                        else
                            r = 0;
                    }
                }
            }
            if (r > this.minSize) {
                let color = tinycolor.random();
                if (this.randomColor) {
                    color = tinycolor({
                        h: color.toHsl().h,
                        s: 1,
                        l: r.map(this.minSize, this.maxSize, .1, .9)
                    });
                    if (this.invertColor)
                        color = tinycolor({
                            h: color.toHsl().h,
                            s: 1,
                            l: r.map(this.minSize, this.maxSize, .9, .1)
                        });
                } else {
                    color = tinycolor({
                        h: this.mainColor.toHsl().h,
                        s: 1,
                        l: r.map(this.minSize, this.maxSize, .1, .9)
                    });
                    if (this.invertColor)
                        color = tinycolor({
                            h: this.mainColor.toHsl().h,
                            s: 1,
                            l: r.map(this.minSize, this.maxSize, .9, .1)
                        });
                }
                context.fillStyle = `${color.toHslString()}`;
                // context.fillStyle = `rgb(${r}, ${r}, ${r})`;
                context.beginPath();
                context.arc(x, y, r, 0, 2 * Math.PI);
                context.fill();
                this.circleArray.push({x: x, y: y, r: r});
            }
            this.circles++;
        }

        this.percentage = this.circles / this.maxCircles * 100;
        this.updateStatus();
        if (this.circles < this.maxCircles) {
            requestAnimationFrame(() => {
                this.draw();
            });
        } else {
            this.finished();
        }
    }

    /**
     * Update the progress span.
     */
    updateStatus() {
        $badgeProgress.html(`${this.status} ${this.percentage.toFixed(0).toString()}%`);
    }

    /**
     * Called when generation is finished
     */
    finished() {
        this.percentage = 100;
        console.log(this.name + " Finished " + ((performance.now() - this.startTime) / 1000).toFixed(2) + "s");
        this.status = "finished";
        this.updateStatus();

        $btnGenerate[0].disabled = false;
        $btnSave[0].disabled = false;
    }
}

// JQuery cached elements
let $btnGenerate, $btnSave, $inputWidth, $inputHeight, $selectGenerate, $badgeProgress,
    $spanIteration, $divOptions;

let canvas, context, width, height;

let gpu = new GPU();

let mandelbrot = new Mandelbrot();
let weave = new Weave();
let circlePack = new CirclePacked();

$(() => {
    $btnGenerate = $('#gen-btn-generate');
    $btnSave = $('#gen-btn-save');
    $inputWidth = $('#gen-input-width');
    $inputHeight = $('#gen-input-height');
    $selectGenerate = $('#gen-select-generator');
    $badgeProgress = $('#gen-badge-progress');
    $spanIteration = $('#gen-span-iteration');
    $divOptions = $('#gen-div-options');

    $btnSave[0].disabled = true;

    canvas = document.getElementById('genCanvas');

    height = window.outerHeight;
    width = window.outerWidth;

    canvas.width = width;
    canvas.height = height;

    context = canvas.getContext('2d');

    context.fillStyle = '#000000';
    context.fillRect(0, 0, width, height);

    // Selection to choose a generator.
    $selectGenerate.on('change', function () {
        console.log($selectGenerate.val());
        switch ($selectGenerate.val()) {
            case "iterative-mandelbrot":
                mandelbrot.loadOptions();
                break;
            case "chaos-weave":
                weave.loadOptions();
                break;
            case "chaos-circle-pack":
                circlePack.loadOptions();
                break;
        }
    });

    // Generate button click event, start a generator
    $btnGenerate.click(function () {
        this.disabled = true;
        $btnSave[0].disabled = true;

        height = $inputHeight.val();
        width = $inputWidth.val();

        canvas.width = width;
        canvas.height = height;

        let render;
        switch ($selectGenerate.val()) {
            case "iterative-mandelbrot":
                render = mandelbrot;
                break;
            case "chaos-weave":
                render = weave;
                break;
            case "chaos-circle-pack":
                render = circlePack;
                break;
        }
        render.init(width, height);
    });

    // Create a new tab, then load data into it. If done the other way it may fail.
    $btnSave.click(() => {
        console.log('Save');
        let w = window.open('about:blank', 'image from canvas');
        let dataURL = canvas.toDataURL('image/png');
        w.document.write("<img src='" + dataURL + "' alt='from canvas'/>");
    });
});

// Helper functions
/**
 * Added to the number type
 * Map this value from a range to a new range. Doesn't change initial value.
 * @param {number} minInput - Input minimum range.
 * @param {number} maxInput - Input maximum range.
 * @param {number} minOutput - Output minimum range.
 * @param {number} maxOutput - Output maximum range.
 * @return {number} - Return mapped number.
 */
Number.prototype.map = function (minInput, maxInput, minOutput, maxOutput) {
    return (this - minInput) * (maxOutput - minOutput) / (maxInput - minInput) + minOutput;
};

/**
 * Random Integer from a range.
 * @param {number} min - Minimum number possible.
 * @param {number} max - Maximum number possible.
 * @return {number} - Random number from range.
 */
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Linear Interpolation
 * @param {number} initialValue - Initial Value, "return initialValue when time === 0".
 * @param {number} endValue - End Value, "return endValue when time === 1".
 * @param {number} time - Time, from 0 to 1.
 */
function lerp(initialValue, endValue, time) {
    return (1 - time) * initialValue + time * endValue;
}

/**
 * Create a input for style options.
 * @param {string} label - Describing span for input.
 * @param {string} id - ID to use in input.
 * @param {number} val - default value of input.
 * @param {number} min - input minimum value.
 * @param {number} max - input maximum value.
 * @param {number} step - input step size.
 * @return {string} returns the html containing the input group.
 */
function createInput(label, id, val, min, max, step) {
    return `
<div class="input-group">        
    <span class="input-group-addon" style="min-width: 175px;">${label}</span>    
    <input id="${id}" class="form-control" type="number" value="${val}" min="${min}" max="${max}" step="${step}">
</div>`;
}

/**
 * Create a slider input for style options.
 * @param {string} label - Describing span for slider.
 * @param {string} id - ID to use in slider input.
 * @param {string} spanId - ID to use in secondary span.
 * @param {number} val - default value of slider.
 * @param {number} min - slider minimum value.
 * @param {number} max - slider maximum value.
 * @param {number} step - slider step size.
 * @return {string} returns the html containing the input group.
 */
function createInputRange(label, id, spanId, val, min, max, step) {
    return `
<div class="input-group">
    <span class="input-group-addon">${label}</span>
    <input id="${id}" style="width: 100%" type="range" value="${val}" min="${min}" max="${max}" step="${step}">
    <span id="${spanId}" class="input-group-addon">${val}</span>
</div>`
}

/**
 * Create a color selector input for style options.
 * @param {string} label - Describing span for color.
 * @param {string} id - ID to use in color input.
 * @param {color} val - default value of color.
 * @return {string} returns the html containing the input group.
 */
function createColorInput(label, id, val) {
    return `
<div class="input-group">
    <span class="input-group-addon" style="min-width: 175px;">${label}</span>
    <span class="input-group-addon" style="width: 100%;">
    <input id="${id}" type="color" value="${val}">
    </span>
</div>
    `;
}

/**
 * Create a checkbox input for style options.
 * @param {string} label - Describing span for checkbox.
 * @param {string} id - ID to use in checkbox.
 * @param {boolean} val - default value of checkbox.
 * @return {string} returns the html containing the input group.
 */
function createCheckbox(label, id, val) {
    return `
<div class="input-group">
    <span class="input-group-addon" style="min-width: 175px;">${label}</span>
    <span class="input-group-addon" style="width: 100%;">
    <input id="${id}" type="checkbox" value="${val}">
    </span>
</div>
    `;
}